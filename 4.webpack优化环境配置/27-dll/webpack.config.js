
const {resolve} = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
//1.引入webpack插件
const webpack = require('webpack')
//3.引入add-asset-html-webpack-plugin插件
const AddAssetHtmlWebpackPlugin = require('add-asset-html-webpack-plugin')

module.exports = {
  entry : './src/index.js',
  output : {
    filename: 'built.js',
    path: resolve(__dirname, 'build')
  },
  plugins: [
    //plugins的配置
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    //2.使用webpack插件告诉webpack哪些库不参与打包，同时使用时的名称也得改变
    new webpack.DllReferencePlugin({
      manifest: resolve(__dirname, 'dll/manifest.json')
    }),
    //4.将该插件指定的文件打包输出，并在html中自动引入该资源
    new AddAssetHtmlWebpackPlugin({
      filepath: resolve(__dirname, 'dll/jquery.js')
    })
  ],
  mode: 'development'
}
