//0.引入path模块解决路径问题
const { resolve } = require('path');
//1.引入插件提取和兼容性处理css文件
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
//2.引入压缩css插件
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
//3.引入处理html图片引入的插件
const HtmlWebpackPlugin = require('html-webpack-plugin');

/**
 * tree shaking:去除无用的代码
 * 前提
 */

//package.json中的browserslist默认使用开发环境，若使用生产环境需要定义nodejs环境变量
process.env.NODE_ENV = 'production';

//复用loader
const commonCssLoader = [
  //这一行作用为将css文件抽离出来
  MiniCssExtractPlugin.loader,
  'css-loader',
  //css兼容性处理
  {
    //还需要在webpack.json中定义browserslist
    loader: 'postcss-loader',
    options: {
      ident: 'postcss',
      //指定插件
      plugins: () => [
        require('postcss-preset-env')()
      ]
    }
  }
]

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'js/built.[contenthash:10].js',
    path: resolve(__dirname, 'build')
  },
  module: {
    rules: [
      //3.进行语法检查
      {
        //在package.json中配置eslintConfig指定检查规则 --> airbnb
        test: /\.js$/,
        //配出不需要语法检查的文件
        exclude: /node_modules/,
        //优先执行
        enforce: 'pre',
        loader: 'eslint-loader',
        options: {
          //自动修复错误
          fix: true
        }
      },
      {
      //oneOf的意思为：以下loader只会匹配一个
      oneOf: [
      //1.处理css文件
      {
        test: /\.css$/,
        //通过扩展运算符使用封装的loader
        use: [...commonCssLoader]
      },
      //2.处理less文件
      {
        test: /\.less$/,
        //由于use数组执行顺序为从下往上(注意执行顺序)，经过less-loader转换为css后再进行兼容性处理
        use: [...commonCssLoader,'less-loader']
      },
      //4.js兼容性处理
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: [
            [
              '@babel/preset-env',
              {
                useBuiltIns: 'usage',
                corejs: {version: 3},
                targets: {
                  chrome: '60',
                  firefox: '50'
                }
              }
            ]
          ],
          //开启babel缓存
          //第二次构建时，会读取之前的缓存
          cacheDirectory: true
        }
      },
      //5.处理图片
      {
        test: /\.(jpg|png|gif)/,
        loader: 'url-loader',
        options: {
          //通过base64编码优化
          limit: 8 * 1024,
          //重命名打包后的图片
          name: '[hash:10].[ext]',
          //指定输出路径
          outputPath: 'imgs'
        }
      },
      //6.处理html中的图片
      {
        test: /\.html$/,
        loader: 'html-loader',
      },
      //8.处理其他文件
      {
        exclude: /\.(js|css|less|html|jpg|png|gif)/,
        //原封不动地输出文件
        loader: 'file-loader',
        options: {
          outputPath: 'media'
        }
      }
        ]
      }
    ]
  },
  plugins: [
    //兼容性处理css并单独抽离css文件
    new MiniCssExtractPlugin({
      //设置输出路径
      filename: 'css/built.[contenthash:10].css',
    }),
    //压缩css
    new OptimizeCssAssetsWebpackPlugin(),
    new HtmlWebpackPlugin({
      //指定html模板
      template: './src/index.html',
      //7.压缩html文件
      minify: {
        //移除空格
        collapseWhitespace: true,
        //移除注释
        removeComments: true
      }
    })
  ],
  //改为production模式自动压缩js文件
  mode: 'production',
  devtool: 'source-map'
}