/**
 * 服务器
 * 启动服务器指令；
 * 方式一：
 * 首先需要全局安装：npm i nodemon -g
 * nodemon server.js
 * 
 * 方式二：
 * node server.js
 * 访问地址：
 * http://localhost:3000
 */
const express = require('express')

const app = express();

app.use(express.static('build', {maxAge: 1000 * 3600}));

app.listen(3000)
