let swap = function(arr, m, n){
  let temp = arr[m]
  arr[m] = arr[n]
  arr[n] = temp
	
}

let media = function (arr){
	let center = Math.floor(arr.length / 2)
    let right = arr.length - 1
    let left = 0
    if(arr[left] > arr[center]){
    	swap(arr, left, center)
    }else if(arr[left] > arr[right]){
    	swap(arr, left, right)
    }else if(arr[center] > arr[right]){
    	swap(arr, center, right)
    }
  return center
}


function foo(arr){
  let center = media(arr)
  let c = arr.splice(center, 1)
  let r = []
  let l = []
  for(let i = 0; i < arr.length; i++ ){
  	if(arr[i] > c){
    	r.push(arr[i])
    }else {
    	l.push(arr[i])
    }
  }
  return foo(l).concat(center, foo(r))
	
}

let arr = [1,55,7,332,54,3]
console.log(foo(arr));
