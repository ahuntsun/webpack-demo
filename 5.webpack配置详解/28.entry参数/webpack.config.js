
const { resolve } = require('path') 
const HtmlWebpackPlugin = require('html-webpack-plugin')

/**
 * entry:入口起点
 * 1.string
 * 2.array
 * 3.object
 */

module.exports = {
  entry: {
    Pindex: ['./src/index.js', './src/count.js'],
    Padd: './src/add.js'
  },
  output: {
    filename: '[name].js',
    path: resolve(__dirname, 'build')
  },
  plugins: [
    new HtmlWebpackPlugin()
  ],
  mode: 'development'
}