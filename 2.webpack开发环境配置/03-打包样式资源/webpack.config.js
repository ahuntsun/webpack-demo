const { resolve } = require('path')

module.exports= {
  entry: './src/index.js',
   //输出:这是一个对象，里面有两个东西。filename表示输出的文件名,path表示输出的路径,写路径的时候通常会写一个绝对路径，避免出错。绝对路径会引入一个nodejs的模块path
  output: {
    filename: 'built.js',
    path: resolve(__dirname, 'build')
  },
  module: {
    rules:[ 
      {
      //详细loader配置
        //匹配哪些文件
        test: /\.css$/,
        //处理这些文件
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      //匹配并处理less文件
      {
       test: /\.less$/,
       use: [
         //以style标签的形式在head标签中插入样式
         'style-loader',
         //将css文件装换为js形式
         'css-loader',
         //将less文件编译成css文件
         'less-loader'
       ]   
      }
    ]
  },
  plugins: [
    //详细plugins配置
  ],
  mode: 'development',
  //mode: 'production'
 }
