
/**
 * 使用dll技术，对某些库(vue、jquery、react等)进行单独打包
 */

const { resolve } = require('path')
//引入webpack插件
const webpack = require('webpack')

module.exports = {
  entry: {
    //最终打包生成的[name] --> jquery
    //['jquery] --> 要打包的库是jquery
    jquery: ['jquery']
  },
  output: {
    //这里的[name]就是上面的jquery
    filename: '[name].js',
    path: resolve(__dirname, 'dll'),
    library: '[name]_[hash]',//打包的库里面向外暴露的内容叫什么名字
  },
  plugins: [
    //该插件作用为：打包生成一个manifest.json文件 --> 提供和jquery的映射
    new webpack.DllPlugin({
      name: '[name]_[hash]',//映射的库暴露的内容名称
      path: resolve(__dirname, 'dll/manifest.json')//输出的名称
    })
  ],
  mode: 'production'
}