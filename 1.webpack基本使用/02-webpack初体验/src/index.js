/*
  index.js:webpack入口起点文件

  1.运行指令：
    开发环境指令：webpack ./src/index.js -o ./build/built.js --mode=development
    翻译：webpack会以./src/index.js为入口环境开始打包，打包后输出 ./build/build.js；整体打包环境时开发环境；
    生产环境指令：
*/

import './index.css';
import data from './data.json';
console.log(data);


function add(x, y){
  return x + y
}

console.log(add(1, 2));
