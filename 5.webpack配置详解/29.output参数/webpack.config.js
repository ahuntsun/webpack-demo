
const { resolve } = require('path') 
const HtmlWebpackPlugin = require('html-webpack-plugin')


module.exports = {
  entry: './src/index.js',
  output: {
    //文件名称（指定名称和目录）
    filename: 'js/[name].js',
    path: resolve(__dirname, 'build'),
    publicPath: '/',
    chunkFilename: 'js/[name]_chunk.js',
    library: '[name]',
    // libraryTarget: 'window'
    libraryTarget: 'commonjs'
  },
  plugins: [
    new HtmlWebpackPlugin()
  ],
  mode: 'development'
}