import { mul } from './test';
import '../css/index.css';

function sum(...args) {
  return args.reduce((p, c) => p + c, 0);
}


// eslint-disable-next-line
console.log(mul(2, 3));
// eslint-disable-next-line
console.log(sum(1, 2, 3, 4));

// 注册Service Worker
// 处理兼容性问题
if ('serviceWorker' in navigator) { // 如果有这个属性，才在页面全局资源加载完后进行注册
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/service-worker.js')
    // 成功
      .then(() => {
        console.log('sw注册成功了');
      })
    // 失败
      .catch(() => {
        console.log('sw注册失败了');
      });
  });
}
