
const { resolve } = require('path') 
const HtmlWebpackPlugin = require('html-webpack-plugin')


module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'js/[name].js',
    path: resolve(__dirname, 'build'),
  },
  module: {
    rules: [
      //loader的配置
      {
        test: /\.css$/,
        //多个loader使用use
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.js$/,
        //配出node_modules下的js文件
        exclude: /node_modules/,
        //只检查src 下的js文件
        include: resolve(__dirname, 'src'),
        //优先执行pre
        // enforce: pre,
        //延后执行post,不写就是按顺序执行
        enforce: post,
        //单个loader用loader
        loader: 'eslint-loader',
        //单个loader时，还可以加一个options进行具体配置
        options: {}
      },
      {
        //表示oneOf中的配置只生效一个
        oneOf: [

        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin()
  ],
  mode: 'development'
}