const { resolve }  = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'built.js',
    path:resolve(__dirname, 'build')
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        //要使用多个loader时使用use数组
        use: [
          'style-loader',
          'css-loader',
          'less-loader'
        ]
      },
      //匹配并处理图片
      {
        test: /\.(jpg|png|gif)$/,
        //只使用一个loader可以不用use数组
        //需要下载url-loader 和 file-loader
        loader: 'url-loader', 
        options: {
          //图片大小小于8kb，就会被base64处理
          //优点：减少请求数量（减轻服务器压力）
          //缺点：图片体积会更大（文件请求速度更慢）
          limit: 8 * 1024,
          name: '[hash:10].[ext]'
        }
      },
      {
        test: /\.html$/,
        //该loader专门处理html中的img图片
        loader: 'html-loader'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }
    )
  ],
  mode: 'development'
}