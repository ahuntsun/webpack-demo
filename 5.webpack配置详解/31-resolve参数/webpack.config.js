
const { resolve } = require('path') 
const HtmlWebpackPlugin = require('html-webpack-plugin')


module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'js/[name].js',
    path: resolve(__dirname, 'build'),
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin()
  ],
  mode: 'development',
  //解析模块的规则
  resolve: {
    //配置解析模块路径别名
    alias: {
      //意思是配置了一个变量$css,它的值就代表了这个css文件的绝对路径
      $css: resolve(__dirname, 'src/css')
    },
    extensions: ['.js', '.json', '.css'],
    //告诉webpack解析模块是去找哪个目录
    modules: [resolve(__dirname, '../../node_modules'), 'node_modules']
  }
}