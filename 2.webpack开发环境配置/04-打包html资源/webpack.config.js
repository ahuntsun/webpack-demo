/**
 * loader: 1、下载 2、使用（配置laoder）
 * plugins: 1、下载 2、引入 3、使用
 */

const {resolve} = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry : './src/index.js',
  output : {
    filename: 'built.js',
    path: resolve(__dirname, 'build')
  },
  module: {
    rules: [
      //laoder的配置
    ]
  },
  plugins: [
    //plugins的配置
    new HtmlWebpackPlugin({
      //复制一个/src/index.html文件，并自动引入打包输出的所有资源
      template: './src/index.html'
    })
  ],
  //为了方便调试，使用不压缩代码的development模式
  mode: 'development'
}
